use std::fs::read;
use std::vec::Vec;
use std::process::exit;

pub enum TokenType
{
    Command(String),
    String(String),
    Integer(i64),
    Float(f64),
    Tab,
    Error(String)
}

pub struct Token
{
    token_type: TokenType,
    loc: Location
}

#[derive(Clone, Copy)]
pub struct Location
{
    pub line: usize,
    pub col: usize
}

pub struct TokenList
{
    list: Vec<Token>,
    index: usize,
    first: bool
}

struct CharList
{
    list: Vec<u8>,
    index: usize,
    first: bool
}

impl TokenList
{
    fn get(&self) -> &Token
    {
        &self.list[self.index]
    }

    pub fn next(&mut self) -> Option<&Token>
    {
        if !self.first
        {
            self.index += 1;
        }
        self.first = false;
        if self.index < self.list.len()
        {
            Some(&self.list[self.index])
        }
        else
        {
            None
        }
    }

    pub fn back(&mut self)
    {
        self.index -= 1;
        if self.index < 0
        {
            self.index = 0;
        }
    }

    fn new() -> TokenList
    {
        TokenList
        {
            list: Vec::<Token>::new(),
            index: 0,
            first: true
        }
    }

    fn push(&mut self, t: Token)
    {
        self.list.push(t)
    }
}

impl CharList
{
    fn get(&self) -> u8
    {
        self.list[self.index]
    }

    fn next(&mut self) -> Option<u8>
    {
        if !self.first
        {
            self.index += 1;
        }
        self.first = false;
        if self.index < self.list.len()
        {
            Some(self.list[self.index])
        }
        else
        {
            None
        }
    }

    fn new() -> CharList
    {
        CharList
        {
            list: Vec::<u8>::new(),
            index: 0,
            first: true
        }
    }

    fn from(l: Vec<u8>) -> CharList
    {
        CharList
        {
            list: l,
            index: 0,
            first: true
        }
    }

    fn push(&mut self, c: u8)
    {
        self.list.push(c)
    }
}




fn next(data: &mut CharList, line: &mut usize, col: &mut usize) -> Option<u8>
{
    loop
    {
        match data.next()
        {
            Some(v) =>
            {
                if v == 0xA
                {
                    *line += 1;
                    *col = 0;
                }
                else if v == 0xD
                {
                    continue;
                }
                else
                {
                    *col += 1;
                }
                return Some(v);
            },
            None =>
            {
                return None;
            }
        }
    }
}

pub fn lex(path: &String, debug: bool) -> Result<TokenList, Vec<(String, Location)>>
{
    let file = read(&path);
    let data: Vec<u8>;
    match file
    {
        Ok(val) => data = val,
        Err(_) =>
        {
            println!("File {} cannot be found", path);
            exit(-1);
        }
    }

    let mut list = CharList::from(data);
    let mut token_list = TokenList::new();
    
    let mut line: usize = 1;
    let mut col: usize = 0;

    loop
    {
        let mut c: char;
        match next(&mut list, &mut line, &mut col)
        {
            Some(val) => c = val as char,
            None => break
        }

        let loc = Location
        {
            line: line,
            col: col
        };
        
        if c > (0x30 as char) && c < (0x3A as char) || c == '-'
        {
            let mut is_float = false;
            let mut val = String::new();
            let mut read = false;
            loop
            {
                if read
                {
                    match next(&mut list, &mut line, &mut col)
                    {
                        Some(v) => c = v as char,
                        None => c = '\n'
                    }
                }
                read = true;
                if c == '\n'
                {
                    col = 0;
                    line += 1;
                    if is_float
                    {
                        let res = val.parse::<f64>();
                        let mut float: f64 = 0.0;
                        let mut error = String::new();
                        match res
                        {
                            Ok(v) => float = v,
                            Err(v) => error = v.to_string()
                        }
                        let token: Token;
                        if error.len() > 0
                        {
                            token = Token
                            {
                                token_type: TokenType::Error(error),
                                loc: loc
                            };
                        }
                        else
                        {
                            token = Token
                            {
                                token_type: TokenType::Float(float),
                                loc: loc
                            };
                        }
                        token_list.push(token);
                        break;
                    }
                    else
                    {
                        let res = val.parse::<i64>();
                        let mut int: i64 = 0;
                        let mut error = String::new();
                        match res
                        {
                            Ok(v) => int = v,
                            Err(v) => error = v.to_string()
                        }
                        let token: Token;
                        if error.len() > 0
                        {
                            token = Token
                            {
                                token_type: TokenType::Error(error),
                                loc: loc
                            };
                        }
                        else
                        {
                            token = Token
                            {
                                token_type: TokenType::Integer(int),
                                loc: loc
                            };
                        }
                        token_list.push(token);
                        break;
                    }
                }
                else if c as char == '.'
                {
                    is_float = true;
                    val.push(c);
                }
                else if c > (0x30 as char) && c < (0x3A as char) || c == '-'
                {
                    val.push(c);
                }
            }
        }
        else if c == '"'
        {
            let mut val = String::new();
            loop
            {
                match next(&mut list, &mut line, &mut col)
                {
                    Some(v) => c = v as char,
                    None => c = '"'
                }
                if c == '\n'
                {
                    col = 0;
                    line += 1;
                }
                else if c == '\\'
                {
                    match match list.next()
                        {
                            Some(v) => v as char,
                            None => '\\'
                        }
                    {
                        'n' => val.push('\n'),
                        'r' => val.push('\r'),
                        '\\' => val.push('\\'),
                        '"' => val.push('"'),
                        '\n' => 
                        {
                            col = 0;
                            line += 1;
                        },
                        _ =>
                        {
                            let token = Token
                            {
                                token_type: TokenType::Error(format!("Invalid escape sequence \\{}", c)),
                                loc: loc
                            };
                            token_list.push(token);
                            break;
                        }
                    }
                }
                else if c == '"'
                {
                    let token = Token
                    {
                        token_type: TokenType::String(val),
                        loc: loc
                    };
                    token_list.push(token);
                    break;
                }
                else
                {
                    val.push(c);
                }
            }
        }
        else if c == '\t'
        {
            token_list.push(Token
            {
                token_type: TokenType::Tab,
                loc: loc
            });
        }
        else if c == '\n'
        {
            col = 0;
            line += 1;
        }
        else
        {
            let mut val = String::new();
            val.push(c);
            loop
            {
                match next(&mut list, &mut line, &mut col)
                {
                    Some(v) => c = v as char,
                    None => c = '\n'
                }
                match c
                {
                    '\n' =>
                    {
                        line += 1;
                        col = 0;
                        let token = Token
                        {
                            token_type: TokenType::Command(val),
                            loc: loc
                        };
                        token_list.push(token);
                        break;
                    },
                    _ => val.push(c)
                }
            }
        }
    }
    
    let mut errors: Vec<(String, Location)> = Vec::new();

    for token in &token_list.list
    {
        match &token.token_type
        {
            TokenType::Command(v) => if debug
            {
                print!("Command '{}' aa", v);
            },
            TokenType::String(v) => if debug
            {
                print!("String \"{}\"", v);
            },
            TokenType::Integer(v) => if debug
            {
                print!("Integer {}", v);
            },
            TokenType::Float(v) => if debug
            {
                print!("Float {}", v);
            },
            TokenType::Tab => if debug
            {
                print!("Tab");
            },
            TokenType::Error(v) =>
            {
                print!("ERROR: {}", v);
                errors.push((v.to_string(), token.loc));
            }
        }
    }

    if errors.len() > 0
    {
        Err(errors)
    }
    else
    {
        Ok(token_list)
    }
}