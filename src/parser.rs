

use lexer::{self, TokenType, Token};

pub struct AST
{
    root: Node
}

pub enum Node
{
    Command(Command),
    UserCommand(UserCommand),
    ValueCommand(ValueCommand)
}

pub struct Command
{
    name: String,
    value: Box<Node>,
    args: Vec<Node>,
    is_root: bool
}

pub struct UserCommand
{
    name: String,
    value: Box<Node>,
    args: Vec<Node>
}

pub struct ValueCommand
{
    value: Value
}

pub enum Value
{
    String(String),
    Integer(i64),
    Float(i64)
}

impl AST
{
    pub fn new() -> AST
    {
        AST
        {
            root: Node::ValueCommand(ValueCommand
            {
                value: Value::String(String::from(""))
            })
        }
    }
}

pub fn parse(mut tokens: lexer::TokenList) -> AST
{
    let mut ast = AST::new();

    let mut indent_level = 0;
    let mut is_root = true;
    let mut parent: Token;

    loop
    {
        let mut is_next_level = true;
        for i in 0..indent_level
        {
            let token: Token;
            match tokens.next()
            {
                Some(val) => token = Token,
                None =>
                {
                    is_next_level = false;
                    break;
                }
            }
            match token.token_type
            {
                TokenType::Tab => continue,
                _ =>
                {
                    
                }
            }
        }

        let token: Token;
        match tokens.next()
        {
            Some(val) => token = Token,
            None => break
        }
        if is_root
        {
            is_root = false
        }
        else
        {
            
        }
    }
}