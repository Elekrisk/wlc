use std::env;
use std::process::exit;

mod lexer;
mod parser;
use lexer::TokenList;

fn usage()
{
    println!("usage:
wlc [-d] <path>

<path>    The path to the file to parse
-d        Enable debug mode")
}

fn main()
{
    let args: Vec<String> = env::args().collect();
    let mut debug = false;
    let mut path = String::from(".");

    match args.len()
    {
        1 => usage(),
        2 =>
        {
            if args[1] == "-d"
            {
                debug = true;
            }
            else
            {
                path = args[1].clone();
            }
        }
        3 =>
        {
            if args[1] == "-d"
            {
                debug = true;
            }
            else
            {
                println!("Unknown argument {}", args[1]);
                usage();
                exit(-2);
            }
            path = args[2].clone();
        }
        _ =>
        {
            if args[1] == "-d"
            {
                println!("Unknown argument {}", args[3]);
                usage();
                exit(-2);
            }
            else
            {
                println!("Unknown argument {}", args[2]);
                usage();
                exit(-2);
            }
        }
    }
    
    let tokens: TokenList;
    match lexer::lex(&path, debug)
    {
        Ok(val) => tokens = val,
        Err(val) =>
        {
            for (msg, loc) in val
            {
                println!("Error: {}\nat ({}, {})", msg, loc.line, loc.col);
            }
            exit(-3);
        }
    };
    let ast = parser::parse(tokens);
}